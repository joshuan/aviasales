const pad = require('pad');
const data = require('./subscriptions.json');

const out = data.subscriptions.map(item => {
	return {
		id: `${item.id}`,
		route: `${item.origin.name} => ${item.destination.name}`,
		price: `Руб < ${item.max_price}`,
		duration: `${item.vacation_duration.min}-${item.vacation_duration.max} дней.`,
		status: item.state === 'active' ? '' : 'Приостановлено'
	};
});

const max = {};
out.forEach(item => {
	Object.keys(item).forEach(key => {
		max[key] = Math.max(item[key].length, max[key] || 0);
	});
});

out
	.filter(item => item.status === '')
	.map(item => {
		console.log([
				'id',
				'route',
				'price',
				'duration',
				'status'
			].map(key => {
				return pad(item[key], max[key]);
			}).join('  ')
		);
	});