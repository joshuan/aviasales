const path = require('path');
const fs = require('fs');
const got = require('got');

got('https://woody.aviasales.ru/subscriptions?email=joshuan.chel@gmail.com')
	.then(response => {
		fs.writeFile(path.join(__dirname, 'subscriptions.json'), response.body, function(err) {
		    if (err) {
		    	console.error(err);
		        return false;
		    }
		    console.log("The file was saved!");
		}); 
	});