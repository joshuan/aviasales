const got = require('got');
const data = require('./subscriptions.json');

function makeUrl(item, action, data) {
	const base = `https://woody.aviasales.ru/subscriptions/${item.id}/${action}.jsonp/?callback=a&email=joshuan.chel@gmail.com&subscription=`;
	const param = encodeURI(JSON.stringify(data));
	return base + param;
}

function disable(item) {
	return makeUrl(item, 'freeze', {"active":false});
}

function update(item, data) {
	const x = Object.assign({}, item, data);
	delete x.id;
	delete x.marker;
	delete x.state;

	return makeUrl(item, 'update', {
	    "raw_rules": x
        // {
        //     "marker": "direct",
        //     "months": [
        //         0
        //     ],
        //     "one_way": false,
        //     "max_price": 15000,
        //     "vacation_duration": {
        //         "min": 3,
        //         "max": 19
        //     },
        //     "id": 3041395,
        //     "state": "active",
        //     "currency": "rub",
        //     "destination": {
        //         "iata": "IST",
        //         "name": "Стамбул"
        //     },
        //     "locale": "ru",
        //     "origin": {
        //         "iata": "SVX",
        //         "name": "Екатеринбург"
        //     }
    	// }
	});
}

/*
{
    "id": 2922715,
    "marker": "direct.home.map",
    "state": "iced", // "active"
    "currency": "rub",
    "destination": {"iata": "GR", "name": "Греция"},
    "one_way": false,
    "locale": "ru",
    "max_price": 10000,
    "months": [0, 9, 10, 11, 1, 2],
    "origin": {"iata": "LED", "name": "Санкт-Петербург"},
    "vacation_duration": {"min": 3, "max": 22} 
}
*/

const list = data.subscriptions
	/** Variant 1 - disable all active */
	// .filter(x => x.state === 'active' && x.origin.iata !== 'SVX') 
	// .map(x => disable(x))

	/** Variant 2 - enable all disabled */
	// .filter(x => x.state !== 'active') 
	// .map(x => enable(x))

	/** Variant 3 - change months, duration, price */
	.filter(x => x.state === 'active') 
	.map(x => update(x, {
		"months": [11, 0], 
		"vacation_duration": {"min": 5, "max": 15},
		"max_price": 10001
	}))
;

function r(list, i) {
	got(list[i]).then(() => {
		console.log(`#${i + 1} was action`);
		if (list[i+1]) {
			r(list, i+1);
		}
	});
}

r(list, 0);











